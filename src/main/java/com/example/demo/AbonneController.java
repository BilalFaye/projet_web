package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.util.ArrayList;
import java.util.List;


@Controller

public class AbonneController {

    private com.example.demo.AbonneRepository abonneRepository;

    @Autowired
    public AbonneController(com.example.demo.AbonneRepository abonneRepository) {
        super();
        this.abonneRepository = abonneRepository;
    }

    // Charger l'index la page par défaut
    @GetMapping("/")
    public String getIndex(Model model) {
        model.addAttribute("search", new Recherche()); // Rechercher un abonné par son id
        model.addAttribute("abonnes", abonneRepository.findAll());
        return "issues/index";
    }


    // Listes des abonnés
    @GetMapping("/abonnes")
    public String getAbonnes(Model model) {
        model.addAttribute("search", new Abonne());
        model.addAttribute("abonnes", abonneRepository.findAll());
        return "issues/index";
    }


    // Recherche par id Abonne
    @PostMapping("/searchAbonne")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String searchAbonne(Recherche search) throws Exception {
        // @RequestBody doit être enlvé
        Long id = search.getId();
        return "redirect:/searchAbonne/" + id;
    }


    @GetMapping("/searchAbonne/{id}")
    public String getAbonneById(@PathVariable(value = "id") Long id, Model model) {
        List<Long> ids = new ArrayList<Long>();
        ids.add(id);
        model.addAttribute("search", new Recherche());
        model.addAttribute("abonnes", abonneRepository.findAllById(ids));
        return "issues/index";
    }


    // Page de mise à jour
    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") Long id, Model model) {
        Abonne abonne = abonneRepository.findById(id).get();
        model.addAttribute("abonne", abonne);
        return "issues/update_abonne";
    }


    // Ajouter un abonné
    @PostMapping("/abonnes")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String addAbonne(Abonne abonne, RedirectAttributes redirectAttributes) throws Exception {
        // @RequestBody doit être enlvé
        abonneRepository.save(abonne);
        redirectAttributes.addAttribute("submitted", true);
        return "redirect:/addAbonne";
    }


    // mise à jour employé
    @PostMapping("/updateAbonne")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String update(Abonne abonne, RedirectAttributes redirectAttributes) throws Exception {
        // @RequestBody doit être enlvé
        abonneRepository.save(abonne);
        return "redirect:/";
    }


    // Supprimer un abonné
    @GetMapping("/deleteAbonne/{id}")
    public String deleteById(@PathVariable(value = "id") Long id) {
        abonneRepository.deleteById(id);
        return "redirect:/";
    }


    //formulaire d'ajout
    @GetMapping("/addAbonne")
    public String addAbonne_form(Model model, @RequestParam(name = "submitted", required = false) boolean submitted) {
        model.addAttribute("submitted", submitted);
        model.addAttribute("abonne", new Abonne());
        return "issues/addAbonne";
    }


    // Aide
    @GetMapping("/aide")
    public String aide() {
        return "issues/aide";
    }


}