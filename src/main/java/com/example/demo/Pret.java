package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "prets")
public class Pret {
    private Long id;
    private Long idLivre;
    private Long idAbonne;
    private String debut;
    private String fin;

    public Pret(){
        super();
    }

    public Pret(Long idAbonne, Long idLivre, String debut, String fin){
        this.idAbonne = idAbonne;
        this.idLivre = idLivre;
        this.debut = debut;
        this.fin = fin;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdLivre() {
        return idLivre;
    }

    public void setIdLivre(Long idLivre) {
        this.idLivre = idLivre;
    }

    public Long getIdAbonne() {
        return idAbonne;
    }

    public void setIdAbonne(Long idAbonne) {
        this.idAbonne = idAbonne;
    }

    public String getDebut() {
        return debut;
    }

    public void setDebut(String debut) {
        this.debut = debut;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    @Override
    public String toString() {
        return "pret{" +
                "id=" + id +
                ", idLivre=" + idLivre +
                ", idAbonne=" + idAbonne +
                ", debut='" + debut + '\'' +
                ", fin='" + fin + '\'' +
                '}';
    }
}
