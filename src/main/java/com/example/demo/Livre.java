package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "livres")
public class Livre {
    private Long id;
    private String titre;
    private String auteur;
    private String commentaire;
    private com.example.demo.Abonne abonne;

    public Livre(){
        super();
    }

    public Livre(String titre, String auteur, String commentaire) {
        this.titre = titre;
        this.auteur = auteur;
        this.commentaire = commentaire;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @OneToOne
    public com.example.demo.Abonne getAbonne() {
        return abonne;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public String getTitre() {
        return titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }


    public void setAbonne(com.example.demo.Abonne abonne) {
        this.abonne = abonne;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public String toString() {
        return "Livre{" +
                "idLivre='" + id + '\'' +
                ", titre='" + titre + '\'' +
                ", auteur='" + auteur + '\'' +
                ", commentaire='" + commentaire + '\'' +
                '}';
    }
}
