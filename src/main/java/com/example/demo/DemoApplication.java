package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demoVehicule(PretRepository pretRepository,AbonneRepository abonneRepository, LivreRepository livreRepository){
		return (args) ->{
			Abonne abonne1 = new Abonne("Bob", "ALEXIS", "bob@gmail.com");
			Abonne abonne2 = new Abonne("Alice", "SYLVA", "alice@gmail.com");
			Abonne abonne3 = new Abonne("Mamadou", "CAMARA", "mamdou@gmail.com");
			Abonne abonne4 = new Abonne("Edouard", "MENDY", "edouard@gmail.com");
			Abonne abonne5 = new Abonne("Heri", "CAMARA", "henri10@hotmail.fr");
			abonneRepository.save(abonne1);
			abonneRepository.save(abonne2);
			abonneRepository.save(abonne3);
			abonneRepository.save(abonne4);
			abonneRepository.save(abonne5);

			Livre livre1 = new Livre("Une si longue lettre", "Mariama Ba", "Ce livre est édité par une Sénégalaise");
			Livre livre2 = new Livre("L'étranger", "Albert Camus", "Cet auteur est français");
			Livre livre3 = new Livre("L'enfant noir", "Camara Laye", "Cet auteur est Guinéen");
			Livre livre4 = new Livre("L'alchimiste", "Paulo Coelho", "Cet auteur est Anglais");
			Livre livre5 = new Livre("Discours de la méthode", "Descartes", "Cet auteur est un philosophe Français");

			livreRepository.save(livre1);
			livreRepository.save(livre2);
			livreRepository.save(livre3);
			livreRepository.save(livre4);
			livreRepository.save(livre5);

			Pret pret1 = new Pret(abonne1.getId(), livre1.getId(), "20-01-2020", "20-02-2020");
			Pret pret2 = new Pret(abonne1.getId(), livre2.getId(), "20-04-2020", "20-05-2020");
			Pret pret3 = new Pret(abonne2.getId(), livre3.getId(), "23-02-2020", "20-03-2020");
			Pret pret4 = new Pret(abonne3.getId(), livre4.getId(), "20-01-2020", "20-02-2020");
			Pret pret5 = new Pret(abonne5.getId(), livre5.getId(), "20-01-2020", "20-02-2020");
			pretRepository.save(pret1);
			pretRepository.save(pret2);
			pretRepository.save(pret3);
			pretRepository.save(pret4);
			pretRepository.save(pret5);
			System.out.println(abonne1.toString());

			System.out.println(abonneRepository.findById(new Long(7)).isPresent());
		};
	}
}
