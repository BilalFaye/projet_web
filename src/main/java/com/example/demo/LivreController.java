package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.jws.WebParam;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LivreController {
    private com.example.demo.LivreRepository livreRepository;

    @Autowired
    public LivreController(com.example.demo.LivreRepository livreRepository){
        super();
        this.livreRepository = livreRepository;
    }

    // Listes des livres
    @GetMapping("/livres")
    public String  getLivres(Model model){
        model.addAttribute("search", new Recherche());
        model.addAttribute("livres", livreRepository.findAll());
        return "issues/livre";
    }

    // Recherche par id livre
    @PostMapping("/searchLivre")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String searchLivre(Recherche search) throws Exception{
        // @RequestBody doit être enlvé
        Long id = search.getId();
        return "redirect:/searchLivre/"+id;
    }

    @GetMapping("/searchLivre/{id}")
    public String getLivreById(@PathVariable(value = "id") Long id, Model model){
        List<Long> ids = new ArrayList<Long>();
        ids.add(id);
        model.addAttribute("search", new Recherche());
        model.addAttribute("livres", livreRepository.findAllById(ids)) ;
        return "issues/livre";
    }

    // Ajouter un livre
    @PostMapping("/livres")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String addLivre(Livre livre, RedirectAttributes redirectAttributes) throws Exception{
        livreRepository.save(livre);
        redirectAttributes.addAttribute("submitted", true);
        return "redirect:/addLivre";
    }

    // Mise à jour livre
    @GetMapping("/showFormForUpdateLivre/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") Long id, Model model){
        Livre livre = livreRepository.findById(id).get();
        model.addAttribute("livre", livre);
        return "issues/update_livre";
    }

    @PostMapping("/updateLivre")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String update(Livre livre, RedirectAttributes redirectAttributes) throws Exception{
        // @RequestBody doit être enlvé
        livreRepository.save(livre);
        return "redirect:/livres";
    }

    // Supprimer livre
    @DeleteMapping("/livres/{id}")
    public void deleteLivre(@PathVariable(value = "id") Long id) throws Exception{
        Livre livre = livreRepository.findById(id).get();
        livreRepository.delete(livre);
    }



    //formulaire d'ajout
    @GetMapping("/addLivre")
    public String addLivre_form(Model model, @RequestParam(name = "submitted", required = false) boolean submitted){
        model.addAttribute("submitted", submitted);
        model.addAttribute("livre", new Livre());
        return "issues/addLivre";
    }

    @GetMapping("/deleteLivre/{id}")
    public String deleteById(@PathVariable(value = "id") Long id){
        livreRepository.deleteById(id);return "redirect:/livres";
    }

    // Voir les commentaires sur le livre
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable(value = "id") Long id, Model model){

        model.addAttribute("livre", livreRepository.findById(id).get()) ;
        return "issues/detail";
    }
}
