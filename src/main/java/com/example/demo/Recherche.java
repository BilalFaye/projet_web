package com.example.demo;

public class Recherche {
    private Long id;

    public Recherche(){
        super();
    }
    public Recherche(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Recherche{" +
                "id=" + id +
                '}';
    }
}
