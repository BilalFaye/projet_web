package com.example.demo;

import org.springframework.data.repository.CrudRepository;

public interface AbonneRepository extends CrudRepository<Abonne, Long> {

}
