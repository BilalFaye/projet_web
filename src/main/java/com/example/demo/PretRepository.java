package com.example.demo;

import org.springframework.data.repository.CrudRepository;

import java.util.Iterator;
import java.util.List;

public interface PretRepository extends CrudRepository<Pret, Long> {

}
