package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
public class PretController {
    PretRepository pretRepository;
    AbonneRepository abonneRepository;
    LivreRepository livreRepository;

    @Autowired
    public PretController(PretRepository pretRepository, LivreRepository livreRepository, AbonneRepository abonneRepository){
        super();
        this.pretRepository = pretRepository;
        this.abonneRepository = abonneRepository;
        this.livreRepository = livreRepository;
    }

    // Listes des prets
    @GetMapping("/prets")
    public String  getPrets(Model model){
        model.addAttribute("search", new Recherche());
        model.addAttribute("prets", pretRepository.findAll());
        return "issues/pret";
    }

    // Ajouter un pret
    @PostMapping("/prets")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String addPret(Pret pret, RedirectAttributes redirectAttributes) throws Exception{
        if(livreRepository.findById(pret.getIdLivre()).isPresent() && abonneRepository.findById(pret.getIdAbonne()).isPresent())
        {
            pretRepository.save(pret);
            redirectAttributes.addAttribute("success", true);
            return "redirect:/addPret";
        }

        redirectAttributes.addAttribute("error", true);
        return "redirect:/addPret";
    }

    // Recherche par id pret
    @PostMapping("/searchPret")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String searchLivre(Recherche search) throws Exception{
        // @RequestBody doit être enlvé
        Long id = search.getId();
        return "redirect:/searchPret/"+id;
    }

    @GetMapping("/searchPret/{id}")
    public String getLivreById(@PathVariable(value = "id") Long id, Model model){
        List<Long> ids = new ArrayList<Long>();
        List<Pret> prets = new ArrayList<Pret>();
        for(Pret pret: pretRepository.findAll()){
            if(pret.getIdAbonne() == id)
                prets.add(pret);
        }
        model.addAttribute("search", new Recherche());
        model.addAttribute("prets", prets) ;
        return "issues/pret";
    }


    // Modifier Pret
    @GetMapping("/showFormForUpdatePret/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") Long id, Model model){
        Pret pret = pretRepository.findById(id).get();
        model.addAttribute("pret", pret);
        return "issues/update_pret";
    }

    @PostMapping("/updatePret")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public String update(Pret pret, RedirectAttributes redirectAttributes) throws Exception{
        // @RequestBody doit être enlvé
        pretRepository.save(pret);
        return "redirect:/prets";
    }

    // Supprimer pret
    @DeleteMapping("/prets/{id}")
    public void deletePret(@PathVariable(value = "id") Long id) throws Exception{
        Pret pret = pretRepository.findById(id).get();
        pretRepository.delete(pret);
    }



    //formulaire d'ajout
    @GetMapping("/addPret")
    public String addPret_form(Model model, @RequestParam(name = "success", required = false) boolean success, @RequestParam(name = "error", required = false) boolean error){
        model.addAttribute("success", success);
        model.addAttribute("error", error);
        model.addAttribute("pret", new Pret());
        return "issues/addPret";
    }

    @GetMapping("/deletePret/{id}")
    public String deleteById(@PathVariable(value = "id") Long id){
        pretRepository.deleteById(id);return "redirect:/prets";
    }

    // Détails sur le pret
    @GetMapping("/detailPret/{id}")
    public String detailPret(@PathVariable(value = "id") Long id, Model model){
        model.addAttribute("livre", livreRepository.findById(pretRepository.findById(id).get().getIdLivre()).get()) ;
        model.addAttribute("abonne", abonneRepository.findById(pretRepository.findById(id).get().getIdAbonne()).get()) ;
        return "issues/detailPret";
    }
}
