package com.example.demo;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table(name = "abonnes")


public class Abonne {

    private Long id;
    private String prenom;
    private String nom;
    private String email;
    private List<Livre> livres = new ArrayList<Livre>();


    public Abonne(){
        super();
    }

    public Abonne(String prenom, String nom, String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    @OneToMany(mappedBy="abonne", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    public List<Livre> getLivres() {
        return livres;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    @Override
    public String toString() {
        return "Abonne{" +
                "idAbonne='" + id + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", livres=" + livres +
                '}';
    }
}
